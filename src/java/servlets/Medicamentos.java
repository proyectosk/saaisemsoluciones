/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import conn.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Americo
 */
public class Medicamentos extends HttpServlet {

    //ConectionDB_SQLServer consql = new ConectionDB_SQLServer();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession(true);
        ConectionDB con = new ConectionDB();
        ConectionDB_SQLServer conModula = new ConectionDB_SQLServer();
        try {
            /*
             *Para actualizar Registros
             */

 /*
             *Manda al jsp el id del registro a editar
             */
            if (request.getParameter("accion").equals("editar")) {
                request.getSession().setAttribute("id", request.getParameter("id"));
                response.sendRedirect("editar_proveedor.jsp");
            }
            /*
             *Para eliminar registro
             */
            if (request.getParameter("accion").equals("eliminar")) {
                //consql.conectar();
                con.conectar();
                try {
                    //consql.insertar("delete from TB_Provee where F_ClaPrv = '" + request.getParameter("id") + "' ");
                    con.insertar("delete from provee_all where F_ClaPrv = '" + request.getParameter("id") + "' ");
                } catch (SQLException e) {
                    System.out.println(e.getMessage());

                    out.println("<script>alert('Error al eliminar')</script>");
                    out.println("<script>window.location='catalogo.jsp'</script>");
                }
                con.cierraConexion();
                //consql.cierraConexion();

                out.println("<script>alert('Se elimino el proveedor correctamente')</script>");
                out.println("<script>window.location='catalogo.jsp'</script>");
            }
            /*
             *Guarda Registros
             */
            if (request.getParameter("accion").equals("guardar")) {
                try {
                    String Nombre = "";
                    int TpMed = 0;
                    con.conectar();
                    //conModula.conectar();
                    try {

                        ResultSet rst_prov = con.consulta("SELECT F_ClaPro FROM tb_medica WHERE F_ClaPro='" + request.getParameter("Clave").toUpperCase() + "'");
                        while (rst_prov.next()) {
                            Nombre = rst_prov.getString("F_ClaPro");
                        }
                        if (!(Nombre.equals(""))) {
                            out.println("<script>alert('Ya esta registrado éste Medicamento')</script>");
                            out.println("<script>window.location='medicamento.jsp'</script>");

                        } else {
                            TpMed = Integer.parseInt(request.getParameter("list_medica").toUpperCase());
                            conModula.ejecutar("insert into IMP_ARTICOLI (ART_OPERAZIONE, ART_ARTICOLO, ART_DES, ART_UMI, ART_MIN, ART_PROY) values ('I','" + request.getParameter("Clave").toUpperCase() + "','" + request.getParameter("Descripcion").toUpperCase() + "','PZ','" + request.getParameter("Min").toUpperCase() + "', 'ISSEMyM')");
                            con.insertar("insert into tb_medica values ('" + request.getParameter("Clave").toUpperCase() + "','" + request.getParameter("Descripcion").toUpperCase() + "','A','" + TpMed + "','" + request.getParameter("Costo").toUpperCase() + "','" + request.getParameter("PresPro").toUpperCase() + "','" + request.getParameter("F_Origen").toUpperCase() + "','" + request.getParameter("SAP").toUpperCase() + "');");
                            con.insertar("insert into tb_maxmodula values('" + request.getParameter("Clave").toUpperCase() + "','" + request.getParameter("Max").toUpperCase() + "','" + request.getParameter("Min").toUpperCase() + "','0')");
                        }
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                        out.println("<script>alert('Error: " + e.getMessage() + "')</script>");
                        out.println("<script>window.location='medicamento.jsp'</script>");
                    }
                    //conModula.cierraConexion();
                    con.cierraConexion();

                    out.println("<script>alert('Medicamento capturado correctamente.')</script>");
                    out.println("<script>window.location='medicamento.jsp'</script>");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            if (request.getParameter("accion").equals("Modificar")) {
                try {
                    con.conectar();
                    String Clave = "", Descripcion = "", radiosts = "", radiorigen = "", radiocat = "", radiotipo = "", Costo = "", Incmen = "", Catalogo = "";
                    String Cat1 = "", Cat2 = "", Cat14 = "", Cat17 = "", Cat100 = "";
                    int ban1 = 0, ban2 = 0, ban14 = 0, ban17 = 0, ban217 = 0;

                    Clave = request.getParameter("Clave");
                    Descripcion = request.getParameter("Descripcion");
                    radiosts = request.getParameter("radiosts");
                    radiorigen = request.getParameter("radiorigen");
                    //radiocat = request.getParameter("radiocat");
                    Cat1 = request.getParameter("cat1");
                    Cat2 = request.getParameter("cat2");
                    Cat14 = request.getParameter("cat14");
                    Cat17 = request.getParameter("cat17");
                    //Cat100 = request.getParameter("cat100");
                    radiotipo = request.getParameter("radiotipo");
                    Costo = request.getParameter("Costo");
                    Incmen = request.getParameter("Incmen");

                    /*if(radiocat == null){radiocat = "";}*/
                    //System.out.println("Cat:"+ radiocat);
                    if (Cat1 == null) {
                        Cat1 = "";
                        ban1 = 0;
                    } else {
                        ban1 = 1;
                    }
                    if (Cat2 == null) {
                        Cat2 = "";
                        ban2 = 0;
                    } else {
                        ban2 = 1;
                    }
                    if (Cat14 == null) {
                        Cat14 = "";
                        ban14 = 0;
                    } else {
                        ban14 = 1;
                    }
                    if (Cat17 == null) {
                        Cat17 = "";
                        ban17 = 0;
                    } else {
                        ban17 = 1;
                    }

                    if ((Clave != "") && (Descripcion != "") && (radiosts != "") && (radiorigen != "") && (radiotipo != "") && (Costo != "") && (Incmen != "")) {

                        con.actualizar("UPDATE tb_medica SET F_N217='0' WHERE F_ClaPro='" + Clave + "';");

                        if ((ban2 == 1) && (ban17 == 1)) {
                            ban217 = 1;
                        } else {
                            ban217 = 0;
                        }
                        con.actualizar("UPDATE tb_medica SET F_DesPro='" + Descripcion + "',F_StsPro='" + radiosts + "',F_TipMed='" + radiotipo + "',F_Costo='" + Costo + "',F_Origen='" + radiorigen + "',F_IncMen='" + Incmen + "',F_N1='" + ban1 + "',F_N2='" + ban2 + "',F_N14='" + ban14 + "',F_N17='" + ban17 + "',F_N217='" + ban217 + "' WHERE F_ClaPro='" + Clave + "';");
                        ban217 = 0;
                        out.println("<script>alert('Medicamento Modificado correctamente.')</script>");
                        out.println("<script>window.location='medicamento.jsp'</script>");
                    } else {
                        out.println("<script>alert('Favor de llenar todos los campos')</script>");
                        out.println("<script>window.location='ModiClave.jsp?Clave=" + Clave + "'</script>");
                    }
                    con.cierraConexion();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            if (request.getParameter("accion").equals("ActualizarUnidis")) {
                try {
                    con.conectar();
                    String Destina = "", coordi = "", doctor = "", Id = "", Nombre = "", Local = "", Muni = "", Region = "", Nivel = "";
                    String m1 = "", m2 = "", m3 = "", m4 = "", m5 = "", m6 = "", m7 = "", m8 = "", m9 = "", m10 = "";
                    Id = request.getParameter("Id");
                    Destina = request.getParameter("Destina");
                    coordi = request.getParameter("coordi");
                    doctor = request.getParameter("doctor");
                    m1 = request.getParameter("m1");
                    m2 = request.getParameter("m2");
                    m3 = request.getParameter("m3");
                    m4 = request.getParameter("m4");
                    m5 = request.getParameter("m5");
                    m6 = request.getParameter("m6");
                    m7 = request.getParameter("m7");
                    m8 = request.getParameter("m8");
                    m9 = request.getParameter("m9");
                    m10 = request.getParameter("m10");

                    Local = request.getParameter("localidad");
                    Muni = request.getParameter("municipio");
                    Region = request.getParameter("region");
                    Nivel = request.getParameter("nivel");
                    Nombre = request.getParameter("descrip");

                    con.actualizar("UPDATE tb_unidis SET F_DesUniIS='" + Nombre + "',F_LocUniIS='" + Local + "',F_MunUniIS='" + Muni + "',F_RegUniIS='" + Region + "',F_NivUniIS='" + Nivel + "',F_ClaSap='" + Destina + "',F_CooUniIS='" + coordi + "',F_MedUniIS='" + doctor + "',F_ClaInt1='" + m1 + "',F_ClaInt2='" + m2 + "',F_ClaInt3='" + m3 + "',F_ClaInt4='" + m4 + "',F_ClaInt5='" + m5 + "',F_ClaInt6='" + m6 + "',F_ClaInt7='" + m7 + "',F_ClaInt8='" + m8 + "',F_ClaInt9='" + m9 + "',F_ClaInt10='" + m10 + "' WHERE F_Id='" + Id + "'");
                    out.println("<script>alert('Unidad Modificado correctamente.')</script>");
                    out.println("<script>window.location='facturacion/catalogoUniDis.jsp'</script>");

                    con.cierraConexion();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            if (request.getParameter("accion").equals("Agregar")) {
                try {
                    con.conectar();
                    String Clave = "", Descripcion = "", radiosts = "", radiorigen = "", radiocat = "", radiotipo = "", Costo = "", Incmen = "";
                    String Cat1 = "", Cat2 = "", Cat14 = "", Cat17 = "";
                    int ban1 = 0, ban2 = 0, ban14 = 0, ban17 = 0, ban217 = 0;
                    int Cont = 0;
                    Clave = request.getParameter("Clave");
                    Descripcion = request.getParameter("Descripcion");
                    radiosts = request.getParameter("radiosts");
                    radiorigen = request.getParameter("radiorigen");
                    //radiocat = request.getParameter("radiocat");
                    Cat1 = request.getParameter("cat1");
                    Cat2 = request.getParameter("cat2");
                    Cat14 = request.getParameter("cat14");
                    Cat17 = request.getParameter("cat17");
                    radiotipo = request.getParameter("radiotipo");
                    Costo = request.getParameter("Costo");
                    Incmen = request.getParameter("Incmen");

                    /*if(radiocat == null){
                        radiocat = "";
                    }*/
                    //System.out.println("Cat:"+ radiocat);
                    if (Cat1 == null) {
                        Cat1 = "";
                        ban1 = 0;
                    } else {
                        ban1 = 1;
                    }
                    if (Cat2 == null) {
                        Cat2 = "";
                        ban2 = 0;
                    } else {
                        ban2 = 1;
                    }
                    if (Cat14 == null) {
                        Cat14 = "";
                        ban14 = 0;
                    } else {
                        ban14 = 1;
                    }
                    if (Cat17 == null) {
                        Cat17 = "";
                        ban17 = 0;
                    } else {
                        ban17 = 1;
                    }

                    if ((Clave != "") && (Descripcion != "") && (radiosts != "") && (radiorigen != "") && (radiotipo != "") && (Costo != "") && (Incmen != "")) {

                        ResultSet DClave = con.consulta("SELECT * FROM tb_medica WHERE F_ClaPro='" + Clave + "'");
                        if (DClave.next()) {
                            Cont++;
                        }
                        if (Cont > 0) {
                            out.println("<script>alert('Medicamento Existente.')</script>");
                            out.println("<script>window.location='AltaClave.jsp'</script>");
                        } else {
                            if ((ban2 == 1) && (ban17 == 1)) {
                                ban217 = 1;
                            } else {
                                ban217 = 0;
                            }
                            con.actualizar("INSERT INTO tb_medica VALUES('" + Clave + "','" + Descripcion + "','" + radiosts + "','" + radiotipo + "','" + Costo + "','pz','" + radiorigen + "','','" + radiocat + "','SPGR','0','0','" + Incmen + "','" + ban1 + "','" + ban2 + "','" + ban14 + "','" + ban17 + "','" + ban217 + "')");
                            ban217 = 0;
                            out.println("<script>alert('Medicamento Guardado correctamente.')</script>");
                            out.println("<script>window.location='AltaClave.jsp'</script>");
                        }

                    } else {
                        out.println("<script>alert('Favor de llenar todos los campos')</script>");
                        out.println("<script>window.location='AltaClave.jsp?Clave=" + Clave + "'</script>");
                    }
                    con.cierraConexion();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            if (request.getParameter("accion").equals("Actualizar")) {
                try {
                    conModula.conectar();
                    con.conectar();
                    ResultSet rset = con.consulta("select * from tb_medica");
                    while (rset.next()) {
                        int banMedicamento = 0;
                        ResultSet rset2 = con.consulta("select F_Id, F_Min from tb_maxmodula where F_ClaPro = '" + rset.getString("F_ClaPro") + "'");
                        while (rset2.next()) {
                            banMedicamento = 1;
                        }
                        if (banMedicamento == 0) {
                            try {
                                con.insertar("insert into tb_maxmodula values('" + rset.getString("F_ClaPro") + "','" + request.getParameter("Max" + rset.getString("F_ClaPro").trim()) + "','" + request.getParameter("Min" + rset.getString("F_ClaPro").trim()) + "','0')");
                            } catch (Exception e) {
                            }
                        } else {

                            try {

                                con.insertar("update tb_maxmodula set F_Max = '" + request.getParameter("Max" + rset.getString("F_ClaPro")) + "', F_Min ='" + request.getParameter("Min" + rset.getString("F_ClaPro")) + "' where F_ClaPro='" + rset.getString("F_ClaPro") + "'");
                            } catch (Exception e) {
                            }
                        }
                        rset2 = con.consulta("select F_Id, F_Min from tb_maxmodula where F_ClaPro = '" + rset.getString("F_ClaPro") + "'");
                        int min = 0;
                        while (rset2.next()) {
                            banMedicamento = 1;
                            min = rset2.getInt("F_Min");
                        }
                        conModula.ejecutar("insert into IMP_ARTICOLI (ART_OPERAZIONE, ART_ARTICOLO, ART_DES, ART_UMI, ART_MIN, ART_PROY) values ('I','" + rset.getString("F_ClaPro") + "','" + rset.getString("F_DesPro") + "','PZ','" + min + "', 'ISSEMyM')");
                    }
                    conModula.cierraConexion();
                    con.cierraConexion();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    out.println("<script>alert('Error: " + e.getMessage() + "')</script>");
                    out.println("<script>window.location='medicamento.jsp'</script>");
                }
                out.println("<script>window.location='medicamento.jsp'</script>");
            }
            if (request.getParameter("accion").equals("obtieneProvee")) {
                try {
                    out.println("<script>alert('Se obtuvieron los Proveedores Correctamente')</script>");
                } catch (Exception e) {
                    out.println("<script>alert('Error al obtener proveedores')</script>");
                }
                out.println("<script>window.location='catalogo.jsp'</script>");
            }

            if (request.getParameter("accion").equals("NuevaPzxCaja")) {
                try {

                    con.conectar();
                    int Contar = 0, Contar2 = 0;
                    
                    String Clave = request.getParameter("Clave");
                    String Nombre = request.getParameter("Nombre");
                    
                    if (Nombre != "") {
                        ResultSet CvE = con.consulta("SELECT COUNT(F_ClaPro) FROM tb_medica WHERE F_ClaPro='" + Clave + "'");
                        if (CvE.next()) {
                            Contar = CvE.getInt(1);
                        }
                        ResultSet CvE2 = con.consulta("SELECT COUNT(F_ClaPro) FROM tb_pzxcaja WHERE F_ClaPro='" + Clave + "'");
                        if (CvE2.next()) {
                            Contar2 = CvE2.getInt(1);
                        }
                        if (Contar2 == 0) {

                            if (Contar > 0) {
                                con.actualizar("INSERT INTO tb_pzxcaja VALUES(0,'" + Clave + "','" + Nombre + "');");
                                out.println("<script>alert('Datos Ingresado Correctamente')</script>");
                                out.println("<script>window.location='PzxCajas.jsp'</script>");
                            } else {
                                out.println("<script>alert('Clave Inexistente, Favor de revisar el Catálogo')</script>");
                                out.println("<script> window.history.back();</script>");
                            }
                        } else {
                            out.println("<script>alert('Clave Registrada anteriormente. Favor de modificar')</script>");
                            out.println("<script> window.history.back();</script>");
                        }
                    } else {
                        out.println("<script>alert('Favor llenar Campo Piezas')</script>");
                        out.println("<script> window.history.back();</script>");
                    }
                    con.cierraConexion();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    out.println("<script>alert('Error: " + e.getMessage() + "')</script>");
                }
            }

            if (request.getParameter("accion").equals("ActuPzxCaja")) {
                try {

                    con.conectar();
                    String Clave = request.getParameter("Clave1");
                    String Cant = request.getParameter("CantN");
                    if (Cant != "") {
                        con.actualizar("UPDATE tb_pzxcaja SET F_Pzs='" + Cant + "' WHERE F_ClaPro='" + Clave + "';");
                        out.println("<script>alert('Datos Actualizados Correctamente')</script>");
                        out.println("<script>window.location='PzxCajas.jsp'</script>");
                    } else {
                        out.println("<script>alert('Favor llenar Campo Nueva Cantidad')</script>");
                        out.println("<script> window.history.back();</script>");
                    }
                    con.cierraConexion();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    out.println("<script>alert('Error: " + e.getMessage() + "')</script>");
                }
            }

        } catch (SQLException e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
