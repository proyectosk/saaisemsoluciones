<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%
    HttpSession sesion = request.getSession();
    String usua = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    String Fecha = request.getParameter("Fecha");
    int Ban = Integer.parseInt(request.getParameter("Ban"));
    String Nombre="";
    if(Ban == 0){
        Nombre = "ConcentradoRuta_Globla";
    }else if(Ban == 1){
        Nombre = "ConcentradoRuta_CSR";
    }else if(Ban == 2){
        Nombre = "ConcentradoRuta_CSU";
    }else if(Ban == 3){
        Nombre = "ConcentradoRuta_CEAPS";
    }
    
    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment;filename=\""+Nombre+".xls");
%>
<div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-condensed table-striped" id="tablaMovMod">
            <thead>
                <tr class="text-center">
                    <td>Clave</td>
                    <td>Descripci&oacute;n</td>
                    <td>Lote</td>
                    <td>Caducidad</td>
                    <td>Cantidad</td>
                    <td>Ubicaci&oacute;n</td>                    
                </tr>
            </thead>
                <tbody>
                    <%
                        try {
                            con.conectar();
                            try {
                    ResultSet rset = null;
                    if(Ban == 0){
                        rset = con.consulta("SELECT l.F_ClaPro,m.F_DesPro,l.F_ClaLot,DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad,SUM(f.F_CantSur+0) as F_Cant,f.f_ubicacion  "
                            + "FROM (SELECT @rownum:=0) as rownum,tb_factura f,	tb_lote l,tb_uniatn u,tb_pzxcaja p,tb_medica m "
                            + "WHERE f.F_Lote = l.F_FolLot AND f.F_ClaCli = u.F_ClaCli AND p.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = l.F_ClaPro AND f.f_ubicacion=l.f_ubica AND f.F_FecEnt = '"+Fecha+"' AND F_CantSur>0 "
                            + "group by l.F_ClaPro, l.F_ClaLot, l.F_FecCad;");
                    }else if(Ban == 1){
                        rset = con.consulta("SELECT l.F_ClaPro,m.F_DesPro,l.F_ClaLot,DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad,SUM(f.F_CantSur+0) as F_Cant,f.f_ubicacion  "
                            + "FROM (SELECT @rownum:=0) as rownum,tb_factura f,	tb_lote l,tb_uniatn u,tb_pzxcaja p,tb_medica m "
                            + "WHERE f.F_Lote = l.F_FolLot AND f.F_ClaCli = u.F_ClaCli AND p.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = l.F_ClaPro AND f.f_ubicacion=l.f_ubica AND f.F_FecEnt = '"+Fecha+"' AND F_CantSur>0 AND u.F_Tipo='RURAL' "
                            + "group by l.F_ClaPro, l.F_ClaLot, l.F_FecCad;");                            
                    }else if(Ban == 2){
                        rset = con.consulta("SELECT l.F_ClaPro,m.F_DesPro,l.F_ClaLot,DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad,SUM(f.F_CantSur+0) as F_Cant,f.f_ubicacion  "
                            + "FROM (SELECT @rownum:=0) as rownum,tb_factura f,	tb_lote l,tb_uniatn u,tb_pzxcaja p,tb_medica m "
                            + "WHERE f.F_Lote = l.F_FolLot AND f.F_ClaCli = u.F_ClaCli AND p.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = l.F_ClaPro AND f.f_ubicacion=l.f_ubica AND f.F_FecEnt = '"+Fecha+"' AND F_CantSur>0 AND u.F_Tipo='CSU' "
                            + "group by l.F_ClaPro, l.F_ClaLot, l.F_FecCad;");                         
                    }else if(Ban == 3){
                        rset = con.consulta("SELECT l.F_ClaPro,m.F_DesPro,l.F_ClaLot,DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad,SUM(f.F_CantSur+0) as F_Cant,f.f_ubicacion  "
                            + "FROM (SELECT @rownum:=0) as rownum,tb_factura f,	tb_lote l,tb_uniatn u,tb_pzxcaja p,tb_medica m "
                            + "WHERE f.F_Lote = l.F_FolLot AND f.F_ClaCli = u.F_ClaCli AND p.F_ClaPro = l.F_ClaPro AND m.F_ClaPro = l.F_ClaPro AND f.f_ubicacion=l.f_ubica AND f.F_FecEnt = '"+Fecha+"' AND F_CantSur>0 AND u.F_Tipo='CEAPS' "
                            + "group by l.F_ClaPro, l.F_ClaLot, l.F_FecCad;");                         
                    }
                    
                    while (rset.next()) {
                        
                                              
                    %>
                    <tr>
                    <td style="mso-number-format:'@'"><%=rset.getString(1)%></td>
                    <td><%=rset.getString(2)%></td>
                    <td style="mso-number-format:'@'"><%=rset.getString(3)%></td>
                    <td><%=rset.getString(4)%></td>
                    <td class="text-center" style="mso-number-format:'#,##0'"><%=rset.getString(5)%></td>
                    <td><%=rset.getString(6)%></td>
                    
                </tr>
                
                    <%
                       }
                            } catch (Exception e) {

                            }
                            con.cierraConexion();
                        } catch (Exception e) {

                        }
                    %>

                </tbody>
            </table>
        </div>
    </div>
</div>