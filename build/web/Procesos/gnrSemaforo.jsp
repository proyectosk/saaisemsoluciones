<%-- 
    Document   : descargaInventario
    Created on : 28/11/2014, 08:45:23 AM
    Author     : Americo
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="conn.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formatter2 = new DecimalFormat("#,###,###.##");
    String Kardex ="";
    try{
    Kardex = request.getParameter("Kardex");
    }catch (Exception e){
        System.out.println(e.getMessage());
    }
    String Semaforo="";
    if(Kardex.equals("1")){
        Semaforo="Menor a 6 meses";
    }else{
        Semaforo="Mayor a 6 meses";
    }
    ConectionDB con = new ConectionDB();
    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment;filename=\"Semaforizacion_"+df2.format(new Date())+"_.xls\"");
%>
<!DOCTYPE html>
<table border="1">
    <tr>
        <td colspan="8" class="text-center" style="alignment-adjust: central"><%=Semaforo%></td>
    </tr>
    <tr>
        <td>Clave SGW</td>
        <td>Clave SAP</td>
        <td>Descripci&oacute;n</td>
        <td>Lote</td>
        <td>Caducidad</td>                                
        <td>Cantidad</td>
        <td>Costo U.</td>
        <td>Monto</td>
        <td>Origen</td>
    </tr>
    <%
        try {
            con.conectar();
            ResultSet rset = null;
            if (Kardex.equals("1")) {
                rset = con.consulta("SELECT l.F_ClaPro, m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad, SUM(F_ExiLot), (m.F_Costo*SUM(l.F_ExiLot)) as monto,m.F_Costo,l.F_Origen,m.F_ClaSap FROM tb_lote l, tb_medica m, tb_ubica u WHERE m.F_ClaPro = l.F_ClaPro AND l.F_Ubica = u.F_ClaUbi AND F_ExiLot != 0 AND F_FecCad < DATE_ADD(CURDATE(), INTERVAL 6 MONTH) GROUP BY l.F_ClaPro,l.F_ClaLot,l.F_FecCad,l.F_Origen");
            } /*else if (Kardex.equals("2")) {
             rset = con.consulta("SELECT l.F_ClaPro, m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad, SUM(F_ExiLot), (m.F_Costo*SUM(l.F_ExiLot)) as monto,m.F_Costo FROM tb_lote l, tb_medica m, tb_ubica u WHERE m.F_ClaPro = l.F_ClaPro AND l.F_Ubica = u.F_ClaUbi AND F_ExiLot != 0 AND F_FecCad BETWEEN DATE_ADD(CURDATE(), INTERVAL 9 MONTH) AND DATE_ADD(CURDATE(), INTERVAL 12 MONTH) GROUP BY l.F_ClaPro,l.F_ClaLot,l.F_FecCad");
             }*/ else if (Kardex.equals("2")) {
                rset = con.consulta("SELECT l.F_ClaPro, m.F_DesPro, l.F_ClaLot, DATE_FORMAT(l.F_FecCad, '%d/%m/%Y') AS F_FecCad, SUM(F_ExiLot), (m.F_Costo*SUM(l.F_ExiLot)) as monto,m.F_Costo,l.F_Origen,m.F_ClaSap FROM tb_lote l, tb_medica m, tb_ubica u WHERE m.F_ClaPro = l.F_ClaPro AND l.F_Ubica = u.F_ClaUbi AND F_ExiLot != 0 AND F_FecCad > DATE_ADD(CURDATE(), INTERVAL 6 MONTH) GROUP BY l.F_ClaPro,l.F_ClaLot,l.F_FecCad,l.F_Origen");
            }
            while (rset.next()) {
    %>
    <tr>
        <td style="mso-number-format:'@';"><%=rset.getString(1)%></td>
        <td style="mso-number-format:'@';"><%=rset.getString(9)%></td>
        <td><%=rset.getString(2)%></td>
        <td style="mso-number-format:'@';"><%=rset.getString(3)%></td>
        <td><%=rset.getString(4)%></td>
        <td><%=formatter.format(rset.getInt(5))%></td>
        <td><%=formatter2.format(rset.getDouble(7))%></td>
        <td><%=formatter2.format(rset.getDouble(6))%></td>
        <td><%=formatter2.format(rset.getDouble(8))%></td>
    </tr>
    <%
            }
            con.cierraConexion();
        } catch (Exception e) {

        }
    %>
</table>