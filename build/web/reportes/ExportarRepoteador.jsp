<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formatterDecimal = new DecimalFormat("#,###,##0.00");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator('.');
    custom.setGroupingSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    formatterDecimal.setDecimalFormatSymbols(custom);
    HttpSession sesion = request.getSession();
    String usua = "",F_User="",FolCon1="";
    String FolCon="",Reporte="",f1="",f2="";
    int ban=0;
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
    } else {
        response.sendRedirect("index.jsp");
    }
    try {
        FolCon = request.getParameter("User");
    } catch (Exception e) {
    }
    ConectionDB con = new ConectionDB();
    
    try{
        ban = Integer.parseInt(request.getParameter("ban"));
        f1 = request.getParameter("f1");
        f2 = request.getParameter("f2");
    }catch(Exception e){}
    
    
    try {
        con.conectar();
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=Reporteador"+f1+"_al_"+f2+".xls");
       
%>
<div>    
    <br />
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-striped" id="datosCompras" border="1">
                <thead>
                    <tr>
                        <%
                        String Columnas="";    
                        for(int x=1; x<=ban; x++){
                            if(!(Columnas == "")){
                                Columnas = Columnas+",F_Colum"+x;
                            }else{
                                Columnas = "F_Colum"+x ;
                            }

                        %>
                        <td>Columna<%=x%></td>
                        <%}%>
                    </tr>
                </thead>
                <tbody>
                    <%
                        
                        ResultSet rset = con.consulta("SELECT "+Columnas+" FROM tb_reporteador WHERE F_User='"+usua+"'; ");
                        while (rset.next()) {
                    %>
                    <tr>                        
                        <%for(int x=1; x<=ban; x++){%>
                        <td><%=rset.getString("F_Colum"+x)%></td>
                        <%}%>
                    </tr>
                    <%
                                }
                            
                            } catch (Exception e) {

                            }
                            con.cierraConexion();
                        } catch (Exception e) {

                        }
                    %>
                </tbody>
            </table>
        </div>
    </div>
</div>
       