<%-- 
    Document   : cambioFechas
    Created on : 14/04/2015, 12:58:35 PM
    Author     : Americo
--%>

<%@page import="javax.print.PrintServiceLookup"%>
<%@page import="javax.print.PrintService"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%

    HttpSession sesion = request.getSession();
    String usua = "";
    String tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();

    String fol_gnkl = "", fol_remi = "", orden_compra = "", fecha = "";
    try {
        if (request.getParameter("accion").equals("buscar")) {
            fol_gnkl = request.getParameter("fol_gnkl");
            fol_remi = request.getParameter("fol_remi");
            orden_compra = request.getParameter("orden_compra");
            fecha = request.getParameter("fecha");
        }
    } catch (Exception e) {

    }
    if (fol_gnkl == null) {
        fol_gnkl = "";
        fol_remi = "";
        orden_compra = "";
        fecha = "";
    }
    String fecha_ini="",fecha_fin="",clave="",ClaCli="";
    try {
        fecha_ini = request.getParameter("fecha_ini");        
        fecha_fin = request.getParameter("fecha_fin");
        clave = request.getParameter("clave");        
        ClaCli = request.getParameter("conceptos");        
    } catch (Exception e) {

    }
    if(fecha_ini==null){
        fecha_ini="";
    }
    if(fecha_fin==null){
        fecha_fin="";
    }
    if(clave == null){
        clave="";
    }
    if (ClaCli == null){
        ClaCli="";
    }
    
    

    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/navbar-fixed-top.css" rel="stylesheet">
        <link href="../css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap.css">
        <!---->
        <title>SIE Sistema de Ingreso de Entradas</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>

            <%@include file="../jspf/menuPrincipal.jspf"%>
            
            <div class="panel-heading">
                <h3 class="panel-title">Consultas Movimientos</h3>
            </div>
            <form action="ConsultasMovi.jsp" method="post">
            <div class="panel-footer">
                <div class="row">
                    <label class="control-label col-sm-1" for="fecha_ini">Clave</label>
                    <div class="col-lg-2">
                        <input class="form-control" id="clave" name="clave" type="text" value=""  />
                    </div>                    
                    <div class="col-lg-2">
                        <input class="form-control" id="fecha_ini" name="fecha_ini" type="date" />
                    </div>
                    <div class="col-lg-2">
                        <input class="form-control" id="fecha_fin" name="fecha_fin" type="date"/>
                    </div>
                    <div class="col-sm-5">
                        <select class="form-control" name="ClaCli" id="ClaCli">
                            <option value="">-Seleccione Concepto Mov.-</option>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("SELECT F_IdCon,CONCAT('[',F_IdCon,']  ',F_DesCon) AS F_DesCon FROM tb_coninv where F_IdCon<1000;");
                                    while (rset.next()) {
                            %>
                            <option value="<%=rset.getString(1)%>"><%=rset.getString(2)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                    out.println(e.getMessage());
                                }
                            %>
                        </select>
                    </div>
                </div>   
            </div>
            <div class="panel-footer">
                <div class="row">
                    <label class="control-label col-sm-1" for="fecha_ini">Conceptos</label>
                    <div class="col-lg-8">
                        <input class="form-control" id="conceptos" name="conceptos" type="text" readonly=""  />                        
                    </div>
                    <div class="col-sm-2">
                        <a href="ConsultasMovi.jsp" class="btn btn-block btn-danger glyphicon glyphicon-trash">Limpiar</a>
                    </div>
                    
                </div>   
            </div>            
                <div class="panel-body">
                    <div class="row ">
                            <button class="btn btn-block btn-success" id="btn_capturar">MOSTRAR&nbsp;<label class="glyphicon glyphicon-search"></label></button>                        
                    </div>
                    
                </div>  
            </form>
            <%
            int Contar=0;
            try {
                con.conectar();
                try {
                    String FechaFol="",Clave="", Concep="",Query="";
                    int ban=0,ban1=0,ban2=0;
                    if (clave !=""){
                        ban=1;
                        Clave = " M.F_ProMov='"+clave+"' ";
                    }
                    if ( fecha_ini !="" && fecha_fin !=""){
                        ban1=1;
                        FechaFol = " M.F_FecMov between '"+fecha_ini+"' and '"+fecha_fin+"' ";
                    }
                    if(ClaCli != ""){
                        ban2=1;
                        Concep = " M.F_ConMov IN ("+ClaCli+") ";
                    }
                    if (ban == 1 && ban1 == 1 && ban2 == 1){
                        Query = Clave+" AND "+FechaFol+" AND "+Concep;
                    }else if (ban == 1 && ban1 == 1){
                        Query = Clave+" AND "+FechaFol;
                    }else if (ban == 1 && ban2 == 1){
                        Query = Clave+" AND "+Concep;
                    }else if (ban1 == 1 && ban2 == 1){
                        Query = FechaFol+" AND "+Concep;
                    }else if (ban == 1){
                        Query = Clave;
                    }else if (ban1 == 1){
                        Query = FechaFol;
                    }else if (ban2 == 1){
                        Query = Concep;
                    }    
                    
                    ResultSet rset = con.consulta("select COUNT(F_ProMov) from tb_movinv M WHERE "+Query+";");
                    if (rset.next()) {
                        Contar = rset.getInt(1);
                    }
                } catch (Exception e) {

                }
                con.cierraConexion();
            } catch (Exception e) {

            }

            %>
            <form action="../Facturacion" method="post" id="formCambioFechas">
                <%
                if(Contar > 0){
                %>
                <div class="row">                    
                    <input class="form-control" id="clave1" name="clave1" type="hidden" value="<%=clave%>" />
                    <input class="form-control" id="concep1" name="concep1" type="hidden" value="<%=ClaCli%>" />
                    <input class="form-control" id="fecha_ini1" name="fecha_ini1" type="hidden" value="<%=fecha_ini%>" />
                    <input class="form-control" id="fecha_fin1" name="fecha_fin1" type="hidden" value="<%=fecha_fin%>" />
                    
                </div>
                    <div class="row col-sm-6">
                        <a class="btn btn-block btn-info" href="gnrMov.jsp?fecha_ini=<%=fecha_ini%>&fecha_fin=<%=fecha_fin%>&clave=<%=clave%>&ClaCli=<%=ClaCli%>">Exportar<span class="glyphicon glyphicon-save"></span></a>
                    </div>
                    <%}%>
                    
                    
                <div>
                    <input class="hidden" name="accion" value="recalendarizarRemis"  />
                    <input class="hidden" id="F_FecEnt" name="F_FecEnt" value=""  />
                    <div class="panel panel-primary">
                        <div class="panel-body table-responsive">
                            <div style="width:100%; height:400px; overflow:auto;">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <td>Fecha Mov.</td>
                                        <td>No.Documento</td>
                                        <td>Con/Mov</td>
                                        <td>Des/Mov</td>
                                        <td>Clave</td>
                                        <td>Descripción</td>
                                        <td>Lote</td>
                                        <td>Caducidad</td>
                                        <td>Cantidad</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        try {
                                            con.conectar();
                                            try {
                                                String FechaFol="",Clave="", Concep="",Query="";
                                                int ban=0,ban1=0,ban2=0;
                                                if (clave !=""){
                                                    ban=1;
                                                    Clave = " M.F_ProMov='"+clave+"' ";
                                                }
                                                if ( fecha_ini !="" && fecha_fin !=""){
                                                    ban1=1;
                                                    FechaFol = " M.F_FecMov between '"+fecha_ini+"' and '"+fecha_fin+"' ";
                                                }
                                                if(ClaCli != ""){
                                                    ban2=1;
                                                    Concep = " M.F_ConMov IN ("+ClaCli+") ";
                                                }
                                                if (ban == 1 && ban1 == 1 && ban2 == 1){
                                                    Query = Clave+" AND "+FechaFol+" AND "+Concep;
                                                }else if (ban == 1 && ban1 == 1){
                                                    Query = Clave+" AND "+FechaFol;
                                                }else if (ban == 1 && ban2 == 1){
                                                    Query = Clave+" AND "+Concep;
                                                }else if (ban1 == 1 && ban2 == 1){
                                                    Query = FechaFol+" AND "+Concep;
                                                }else if (ban == 1){
                                                    Query = Clave;
                                                }else if (ban1 == 1){
                                                    Query = FechaFol;
                                                }else if (ban2 == 1){
                                                    Query = Concep;
                                                }
                                                
                                                ResultSet rset = con.consulta("SELECT DATE_FORMAT(M.F_FecMov,'%d/%m/%Y') AS F_FecMov,F_DocMov,F_ConMov,C.F_DesCon,F_ProMov,MD.F_DesPro,l.F_ClaLot,DATE_FORMAT(l.F_FecCad,'%d/%m/%Y') AS F_FecCad,SUM(F_CantMov) AS F_CantMov FROM tb_movinv m INNER JOIN tb_lote l on m.F_ProMov=l.F_ClaPro AND m.F_LotMov=l.F_FolLot AND m.F_UbiMov=l.F_Ubica INNER JOIN tb_medica MD ON M.F_ProMov=MD.F_ClaPro INNER JOIN tb_coninv C ON M.F_ConMov=C.F_IdCon WHERE "+Query+" AND M.F_ConMov < 1000  GROUP BY F_FecMov,F_DocMov,F_ConMov,F_ProMov,l.F_ClaLot,F_FecCad ;");
                                                while (rset.next()) {

                                    %>
                                    <tr>                                        
                                        <td><%=rset.getString(1)%></td>
                                        <td><%=rset.getString(2)%></td>
                                        <td><%=rset.getString(3)%></td>
                                        <td><%=rset.getString(4)%></td>
                                        <td><%=rset.getString(5)%></td>
                                        <td><%=rset.getString(6)%></td>
                                        <td><%=rset.getString(7)%></td>
                                        <td><%=rset.getString(8)%></td>
                                        <td><%=rset.getString(9)%></td>
                                    </tr>
                                    <%
                                                }
                                            } catch (Exception e) {

                                            }
                                            con.cierraConexion();
                                        } catch (Exception e) {

                                        }
                                    %>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        

        
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui-1.10.3.custom.js"></script>
        <script src="../js/bootstrap-datepicker.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function() {
                $('#datosCompras').dataTable();
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});

                //$('#btnRecalendarizar').attr('disabled', true);
                //$('#btnImpMult').attr('disabled', true);
            });
            
        </script>
        <script>
            $(document).ready(function() {
                $('#datosCompras').dataTable();
                $("#fecha").datepicker();
                $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});

                //$('#btnRecalendarizar').attr('disabled', true);
                //$('#btnImpMult').attr('disabled', true);
            });
            
        </script>
        <script>
            $(document).ready(function(){
                $('#ClaCli').change(function(){
                    var Concep = $('#conceptos').val();
                    var valor = $('#ClaCli').val();                    
                     if(Concep !=""){
                     $('#conceptos').val(Concep+","+valor);
                 }else{                        
                         $('#conceptos').val(valor);                      
                     }                     
                 });
            });
       </script>
       
    </body>
</html>

