<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%

    HttpSession sesion = request.getSession();
    String usua = "", Clave = "", tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        Clave = (String) session.getAttribute("clave");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="css/navbar-fixed-top.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIE Sistema de Ingreso de Entradas</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>           
            <%@include file="jspf/menuPrincipal.jspf" %>
            <hr/>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Catálogo de Presentación por Clave</h3>
                </div>
                <div class="panel-body ">
                    <form class="form-horizontal" role="form" name="formulario1" id="formulario1" method="post" action="Medicamentos">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="Clave" class="col-xs-1 control-label">Clave*</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="Clave" name="Clave" placeholder="Clave" onKeyPress="return tabular(event, this)" required="" autofocus >
                                </div>
                                <label for="Nombre" class="col-xs-1 control-label">Piezas*</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control" id="Nombre" name="Nombre" maxlength="60" placeholder="Nombre" required="" onKeyPress="return tabular(event, this)" />
                                </div>                                
                                <div class="col-lg-1"></div>                                
                            </div>
                        </div>                        

                        <button class="btn btn-block btn-primary" type="submit" name="accion" value="NuevaPzxCaja" onclick="return valida_alta();"> Guardar</button> 

                    </form>
                    <div>
                        <h6>Los campos marcados con * son obligatorios</h6>
                    </div>
                </div>
                <div class="panel-footer">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datosProv">
                        <thead>
                            <tr>
                                <td>Clave</td>
                                <td>Descripción</td>
                                <td>PzxCajas</td>
                                <td>Modificar</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("SELECT P.F_ClaPro,M.F_DesPro,P.F_Pzs,P.F_Id FROM tb_pzxcaja P INNER JOIN tb_medica M ON P.F_ClaPro=M.F_ClaPro ORDER BY P.F_ClaPro ASC");
                                    while (rset.next()) {
                            %>
                            <tr class="odd gradeX">
                                <td class="Clave"><small><%=rset.getString(1)%></small></td>
                                <td class="Nombre"><small><%=rset.getString(2)%></small></td>
                                <td class="Pz"><small><%=rset.getString(3)%></small></td>
                                <td>
                                    <a class="btn btn-block btn-warning rowButton" data-toggle="modal" data-target="#Devolucion"><span class="glyphicon glyphicon-pencil"></span></a>
                                </td>
                            </tr>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="Devolucion" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>
                            Modificar Piezas por Cajas
                        </h4>
                    </div>
                    <form name="Medicamentos" action="Medicamentos" method="Post">
                        <div class="modal-body">
                            <div class="row">
                                <h4 class="col-sm-2">Clave Insumo</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Clave1" id="Clave1" type="text" value="" readonly="" required="">
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Descripción</h4>
                                <div class="col-sm-10">
                                    <input class="form-control" name="Nombre1" id="Nombre1" type="text" value="" readonly="" required="">
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Cantidad</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="Cant" id="Cant" type="text" readonly="" value="" required="">
                                </div>
                                <h4 class="col-sm-2">Cantidad Nueva</h4>
                                <div class="col-sm-2">
                                    <input class="form-control" name="CantN" id="CantN" type="text" value="" required="">
                                </div>
                            </div>                           
                        </div>  
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="ActuPzxCaja">Guardar</button>
                            <button type="submit" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>            
        </div>                
        <%@include file="jspf/piePagina.jspf" %>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script>
                            $(document).ready(function () {
                                $('#datosProv').dataTable();
                            });
        </script>
        <script type="text/javascript">
            $(".rowButton").click(function () {
                var $row = $(this).closest("tr");
                var clave = $row.find("td.Clave").text();
                var nombre = $row.find("td.Nombre").text();
                var Pz = $row.find("td.Pz").text();

                $("#Clave1").val(clave);
                $("#Nombre1").val(nombre);
                $("#Cant").val(Pz);

            });


        </script>
        <script>


            function isNumberKey(evt, obj)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode === 13 || charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode === 13) {
                        frm = obj.form;
                        for (i = 0; i < frm.elements.length; i++)
                            if (frm.elements[i] === obj)
                            {
                                if (i === frm.elements.length - 1)
                                    i = -1;
                                break
                            }
                        /*ACA ESTA EL CAMBIO*/
                        if (frm.elements[i + 1].disabled === true)
                            tabular(e, frm.elements[i + 1]);
                        else
                            frm.elements[i + 1].focus();
                        return false;
                    }
                    return false;
                }
                return true;
            }


            function valida_alta() {
                /*var Clave = document.formulario1.Clave.value;*/
                var Nombre = document.formulario1.Nombre.value;
                if (Nombre === "") {
                    alert("Tiene campos vacíos, verifique.");
                    return false;
                }
            }
        </script>
        <script language="javascript">
            function justNumbers(e)
            {
                var keynum = window.event ? window.event.keyCode : e.which;
                if ((keynum == 8) || (keynum == 46))
                    return true;
                return /\d/.test(String.fromCharCode(keynum));
            }
            otro = 0;
            function LP_data() {
                var key = window.event.keyCode; //codigo de tecla. 
                if (key < 48 || key > 57) {//si no es numero 
                    window.event.keyCode = 0; //anula la entrada de texto. 
                }
            }
            function anade(esto) {
                if (esto.value === "(55") {
                    if (esto.value.length === 0) {
                        if (esto.value.length === 0) {
                            esto.value += "(";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 3) {
                            esto.value += ") ";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 9) {
                            esto.value += "-";
                        }
                    }
                    if (esto.value.length < otro) {
                        if (esto.value.length === 4 || esto.value.length === 9) {
                            esto.value = esto.value.substring(0, esto.value.length - 1);
                        }
                    }
                } else {
                    if (esto.value.length === 0) {
                        if (esto.value.length === 0) {
                            esto.value += "(";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 4) {
                            esto.value += ") ";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 9) {
                            esto.value += "-";
                        }
                    }
                    if (esto.value.length < otro) {
                        if (esto.value.length === 4 || esto.value.length === 9) {
                            esto.value = esto.value.substring(0, esto.value.length - 1);
                        }
                    }
                }
                otro = esto.value.length

            }


            function tabular(e, obj)
            {
                tecla = (document.all) ? e.keyCode : e.which;
                if (tecla != 13)
                    return;
                frm = obj.form;
                for (i = 0; i < frm.elements.length; i++)
                    if (frm.elements[i] == obj)
                    {
                        if (i == frm.elements.length - 1)
                            i = -1;
                        break
                    }
                /*ACA ESTA EL CAMBIO*/
                if (frm.elements[i + 1].disabled == true)
                    tabular(e, frm.elements[i + 1]);
                else
                    frm.elements[i + 1].focus();
                return false;
            }

        </script> 

    </body>
</html>

