<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%

    HttpSession sesion = request.getSession();
    String usua = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="css/navbar-fixed-top.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.css">
        <!---->
        <title>SIE Sistema de Ingreso de Entradas</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>
            <!--div class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="main_menu.jsp">Inicio</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Entradas<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="captura.jsp">Entrada Manual</a></li>
                                    <li><a href="compraAuto2.jsp">Entrada Automática OC ISEM</a></li>
                                    <li><a href="reimpresion.jsp">Reimpresión de Compras</a></li>
                                    <li><a href="ordenesCompra.jsp">Órdenes de Compras</a></li>
                                    <li><a href="kardexClave.jsp">Kardex Claves</a></li>
                                    <li><a href="Ubicaciones/Consultas.jsp">Ubicaciones</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Facturación<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="requerimiento.jsp">Carga de Requerimiento</a></li>
                                    <li><a href="factura.jsp">Facturación Automática</a></li>
                                     <li><a href="reimp_factura.jsp">Administrar Remisiones</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Catálogos<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="medicamento.jsp">Catálogo de Medicamento</a></li>
                                    <li><a href="catalogo.jsp">Catálogo de Proveedores</a></li>
                                    <li><a href="marcas.jsp">Catálogo de Marcas</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Fecha Recibo<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Entrega.jsp">Fecha de Recibo en CEDIS</a></li>    
                                    <li><a href="historialOC.jsp">Historial OC</a></li>                                     
                                </ul>
                            </li>
                            
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href=""><span class="glyphicon glyphicon-user"></span> <%=usua%></a></li>
                            <li class="active"><a href="index.jsp"><span class="glyphicon glyphicon-log-out"></span></a></li>
                        </ul>
                    </div><!--/.nav-collapse>
                </div>
            </div-->
            <hr/>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Catalogo de Unidades</h3>
                </div>
                <div class="panel-body ">
                    <form class="form-horizontal" role="form" name="formulario1" id="formulario1" method="post" action="AltaUnidad">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="Clave" class="col-xs-1 control-label">Clave*</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="Clave" name="Clave" placeholder="Clave" onKeyPress="return tabular(event, this)" required="" autofocus >
                                </div>
                                <label for="Nombre" class="col-xs-1 control-label">Nombre*</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control" id="Nombre" name="Nombre" maxlength="60" placeholder="Nombre" required="" onKeyPress="return tabular(event, this)" />
                                </div>                                
                                <div class="col-lg-1"></div>
                                <div class="col-lg-2"><a class="btn btn-block btn-danger" onclick="window.close();">Salir</a></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="Direccion" class="col-xs-1 control-label">Dirección</label>
                                <div class="col-xs-3">
                                    <input type="text" class="form-control" id="Direccion" maxlength="50" name="Direccion" placeholder="Direccion" required="" onKeyPress="return tabular(event, this)" />
                                </div>
                                <label for="CP" class="col-xs-1 control-label">N° Juris</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="juris" name="juris" placeholder="N° Juris" maxlength="5" required="" onKeyPress="return isNumberKey(event, this);" />
                                </div>
                                <label for="CP" class="col-xs-1 control-label">CveMuni</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" id="mun" name="mun" placeholder="Cve Municipio" maxlength="5" required="" onKeyPress="return isNumberKey(event, this);" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="Direccion" class="col-sm-2 control-label">Tipo Unidad</label>
                                <div class="col-xs-1">
                                    <select name="tipo" id="tipo">
                                        <option value="">--Seleccione--</option>
                                        <option value="RURAL">RURAL</option>
                                        <option value="CEAPS">CEAPS</option>
                                        <option value="CSU">CSU</option>
                                        <option value="CAD">CAD</option>
                                        <option value="GEDIATRICAS">GERIATRICAS</option>
                                    </select>
                                </div>
                                <label for="Dispensa" class="col-xs-2 control-label">Dispensador</label>
                                <div class="col-xs-1">
                                    <select name="dispensador" id="dispensador">
                                        <option value="">--Seleccione--</option>
                                        <option value="DIMESA">DIMESA</option>
                                        <option value="DISUR">DISUR</option>
                                        <option value="MEDALFA">MEDALFA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <button class="btn btn-block btn-primary" type="submit" name="accion" value="guardar" onclick="return valida_alta();"> Guardar</button> 

                    </form>
                    <div>
                        <h6>Los campos marcados con * son obligatorios</h6>
                    </div>
                </div>
                <div class="panel-footer">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="datosProv">
                        <thead>
                            <tr>
                                <td>Clave</td>
                                <td>Nombre</td>
                                <td>Sts</td>
                                <td>Direcci&oacute;n</td>
                                <td>Tipo Unidad</td>
                                <td>Dispensador</td>
                                <td>Modificar</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("SELECT * FROM tb_uniatn ORDER BY F_ClaCli ASC");
                                    while (rset.next()) {
                            %>
                            <tr class="odd gradeX">
                                <td class="Clave"><small><%=rset.getString(1)%></small></td>
                                <td class="Nombre"><small><%=rset.getString(2)%></small></td>
                                <td class="Sts"><small><%=rset.getString(3)%></small></td>
                                <td class="Direc"><small><%=rset.getString(8)%></small></td>
                                <td class="Tipo"><small><%=rset.getString(6)%></small></td>
                                <td class="Dispen"><small><%=rset.getString(10)%></small></td>
                                <td>
                                    <a class="btn btn-block btn-warning rowButton" data-toggle="modal" data-target="#Devolucion"><span class="glyphicon glyphicon-pencil"></span></a>
                                </td>
                            </tr>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="Devolucion" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>
                            Modificar Unidad
                        </h4>
                    </div>
                    <form name="AltaUni" action="AltaUnidad" method="Post">
                        <div class="modal-body">
                            <div class="row">
                                <h4 class="col-sm-2">Clave Unidad</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Clave1" id="Clave1" type="text" value="" readonly="" required="">
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Nombre Unidad</h4>
                                <div class="col-sm-10">
                                    <input class="form-control" name="Nombre1" id="Nombre1" type="text" value="" required="">
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Direcci&oacute;n</h4>
                                <div class="col-sm-10">
                                    <input class="form-control" name="Direc1" id="Direc1" type="text" value="" required="">
                                </div>                                
                            </div>
                            <div class="row">
                                <h4 class="col-sm-2">Sts</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Sts1" id="Sts1" type="text" value="" required="">
                                </div>                                
                            </div>  
                            <div class="row">
                                <h4 class="col-sm-2">Tipo Unidad</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="TipoU" id="TipoU" type="text" value="" required="" readonly="">                                    
                                </div>
                                <div class="col-xs-2">
                                    <select name="tipou" id="tipou">
                                        <option value="">--Seleccione--</option>
                                        <option value="RURAL">RURAL</option>
                                        <option value="CEAPS">CEAPS</option>
                                        <option value="CSU">CSU</option>
                                        <option value="CAD">CAD</option>
                                        <option value="GEDIATRICAS">GERIATRICAS</option>
                                    </select>
                                </div>
                            </div>  
                            <div class="row">
                                <h4 class="col-sm-2">Dispensador</h4>
                                <div class="col-sm-4">
                                    <input class="form-control" name="Dispensa" id="Dispensa" type="text" value="" required="" readonly="">
                                </div>
                                <div class="col-xs-2">
                                    <select name="dispensadoru" id="dispensadoru">
                                        <option value="">--Seleccione--</option>
                                        <option value="DIMESA">DIMESA</option>
                                        <option value="DISUR">DISUR</option>
                                        <option value="MEDALFA">MEDALFA</option>
                                    </select>
                                </div>
                            </div>  
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" name="accion" value="Modificar">Guardar</button>
                            <button type="submit" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>            
        </div>                
        <%@include file="jspf/piePagina.jspf" %>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-ui-1.10.3.custom.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap.js"></script>
        <script>
                            $(document).ready(function () {
                                $('#datosProv').dataTable();
                            });
        </script>
        <script type="text/javascript">
            $(".rowButton").click(function (){
               var $row = $(this).closest("tr");
               var clave = $row.find("td.Clave").text(); 
               var nombre = $row.find("td.Nombre").text(); 
               var sts = $row.find("td.Sts").text(); 
               var dir = $row.find("td.Direc").text();
               var tipo = $row.find("td.Tipo").text();
               var dispen = $row.find("td.Dispen").text();

               $("#Clave1").val(clave);
               $("#Nombre1").val(nombre);
               $("#Sts1").val(sts);
               $("#Direc1").val(dir);
               $("#TipoU").val(tipo);
               $("#Dispensa").val(dispen);

           });       
           $('#tipou').change(function() {                    
                    var tipou = $('#tipou').val();
                    if(tipou !=''){
                        $('#TipoU').val(tipou);
                    }                    
                });
                
                $('#dispensadoru').change(function() {                    
                    var dispenu = $('#dispensadoru').val();
                   
                    if(dispenu !=''){
                        $('#Dispensa').val(dispenu);
                    }                    
                });
                
                
       </script>
        <script>


            function isNumberKey(evt, obj)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode === 13 || charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode === 13) {
                        frm = obj.form;
                        for (i = 0; i < frm.elements.length; i++)
                            if (frm.elements[i] === obj)
                            {
                                if (i === frm.elements.length - 1)
                                    i = -1;
                                break
                            }
                        /*ACA ESTA EL CAMBIO*/
                        if (frm.elements[i + 1].disabled === true)
                            tabular(e, frm.elements[i + 1]);
                        else
                            frm.elements[i + 1].focus();
                        return false;
                    }
                    return false;
                }
                return true;
            }


            function valida_alta() {
                /*var Clave = document.formulario1.Clave.value;*/
                var Nombre = document.formulario1.Nombre.value;
                var Telefono = document.formulario1.Telefono.value;
                if (Nombre === "" || Telefono === "") {
                    alert("Tiene campos vacíos, verifique.");
                    return false;
                }
            }
        </script>
        <script language="javascript">
            function justNumbers(e)
            {
                var keynum = window.event ? window.event.keyCode : e.which;
                if ((keynum == 8) || (keynum == 46))
                    return true;
                return /\d/.test(String.fromCharCode(keynum));
            }
            otro = 0;
            function LP_data() {
                var key = window.event.keyCode; //codigo de tecla. 
                if (key < 48 || key > 57) {//si no es numero 
                    window.event.keyCode = 0; //anula la entrada de texto. 
                }
            }
            function anade(esto) {
                if (esto.value === "(55") {
                    if (esto.value.length === 0) {
                        if (esto.value.length === 0) {
                            esto.value += "(";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 3) {
                            esto.value += ") ";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 9) {
                            esto.value += "-";
                        }
                    }
                    if (esto.value.length < otro) {
                        if (esto.value.length === 4 || esto.value.length === 9) {
                            esto.value = esto.value.substring(0, esto.value.length - 1);
                        }
                    }
                } else {
                    if (esto.value.length === 0) {
                        if (esto.value.length === 0) {
                            esto.value += "(";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 4) {
                            esto.value += ") ";
                        }
                    }
                    if (esto.value.length > otro) {
                        if (esto.value.length === 9) {
                            esto.value += "-";
                        }
                    }
                    if (esto.value.length < otro) {
                        if (esto.value.length === 4 || esto.value.length === 9) {
                            esto.value = esto.value.substring(0, esto.value.length - 1);
                        }
                    }
                }
                otro = esto.value.length

            }


            function tabular(e, obj)
            {
                tecla = (document.all) ? e.keyCode : e.which;
                if (tecla != 13)
                    return;
                frm = obj.form;
                for (i = 0; i < frm.elements.length; i++)
                    if (frm.elements[i] == obj)
                    {
                        if (i == frm.elements.length - 1)
                            i = -1;
                        break
                    }
                /*ACA ESTA EL CAMBIO*/
                if (frm.elements[i + 1].disabled == true)
                    tabular(e, frm.elements[i + 1]);
                else
                    frm.elements[i + 1].focus();
                return false;
            }

        </script> 

    </body>
</html>

