<%-- 
    Document   : index
    Created on : 17/02/2014, 03:34:46 PM
    Author     : Americo
--%>

<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formatterDecimal = new DecimalFormat("#,###,##0.00");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator('.');
    custom.setGroupingSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    formatterDecimal.setDecimalFormatSymbols(custom);
    HttpSession sesion = request.getSession();
    String usua = "";
    int Total = 0;
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
    } else {
        response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();

    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment;filename=\"Existencias_Cedis_Mes_Global.xls\"");
    try {
                            con.conectar();
                            ResultSet Consulta = null;
%>
<div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-striped" id="datosCompras" border="1">
                <thead>
                    <tr>
                        <td>Mes</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td style='mso-number-format:"@"'><%=Consulta.getString(1)%></td><%}%>                       
                    </tr>
                    <tr>
                        <td>(+)Inv. Inicio Mes</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(2))%></td><%}%> 
                    </tr>
                    <tr>
                        <td>(+)Entrada Por Inventario Inicial</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(3))%></td><%}%>     
                    </tr>
                    <tr>
                        <td>(+)Entrada Por Compra</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(4))%></td><%}%> 
                    </tr>
                    <tr>
                        <td>(+)Entrada Por Devoluci&oacute;n</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(5))%></td><%}%> 
                    </tr>
                    <tr>                        
                        <td>(+)Entrada Por Otros Movimientos</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(6))%></td><%}%> 
                    </tr>
                    <tr>                        
                        <td>SubTotal Entradas</td><%Consulta = con.consulta("SELECT SUM(F_InvMes+F_InvIni+F_Compra+F_Devo+F_EoMov) FROM tb_invmescedisglobal GROUP BY F_Mes ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(1))%></td><%}%>  
                    </tr>
                    <tr></tr>
                    <tr>                        
                        <td>(-)Salida Por Facturaci&oacute;n</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(7))%></td><%}%> 
                    </tr>
                    <tr>
                        <td>(-)Salida Por Otros Movimientos</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(8))%></td><%}%> 
                    </tr>
                    <tr>                        
                        <td>SubTotal Salidas</td><%Consulta = con.consulta("SELECT SUM(F_SalFact+F_SalOtroMov) FROM tb_invmescedisglobal GROUP BY F_Mes ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(1))%></td><%}%> 
                    </tr>
                    <tr></tr>
                    <tr>
                        <td>Inv. Final Mes</td><%Consulta = con.consulta("SELECT * FROM tb_invmescedisglobal ORDER BY F_Anno,F_Nmes+0;");
                            while (Consulta.next()) {%><td><%=formatter.format(Consulta.getInt(9))%></td><%}%>
                    </tr>                    
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <br />
        <br />
        <br />
        <!--div class="panel panel-primary">
        <div class="panel-body">
            <table class="table table-bordered table-striped" id="datosfirmas" border="0">
                <tr>
                    <td colspan="3"><img src="http://187.176.10.50:8081/SAAISEMSOLUCIONES/imagenes/firmas/juris1/1001A.jpg" width="80" height="100"></td>
                    <td colspan="3"><img src="http://187.176.10.50:8081/SAAISEMSOLUCIONES/imagenes/firmas/juris1/1001A.jpg" width="80" height="100"></td>
                </tr>
                <tr>
                    <td colspan="2"><h5>RESPONSABLE MEDICO</h5></td>
                    <td colspan="3"><h5>COORDINADOR O ADMINISTRADOR MUNICIPAL</h5></td>
                </tr>
            </table>
        </div>
        </div-->


    </div>
</div>
                    <%
                            
                            con.cierraConexion();
                        } catch (Exception e) {

                        }


                    %>