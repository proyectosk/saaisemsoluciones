<%-- 
    Document   : cambioFechas
    Created on : 14/04/2015, 12:58:35 PM
    Author     : Americo
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="javax.print.PrintServiceLookup"%>
<%@page import="javax.print.PrintService"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="conn.*" %>
<!DOCTYPE html>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy"); %>
<%DecimalFormat format = new DecimalFormat("####,###");%>
<%

    HttpSession sesion = request.getSession();
    String usua = "";
    String tipo = "";
    if (sesion.getAttribute("nombre") != null) {
        usua = (String) sesion.getAttribute("nombre");
        tipo = (String) sesion.getAttribute("Tipo");
    } else {
        response.sendRedirect("../index.jsp");
    }
    ConectionDB con = new ConectionDB();

    String fecha_ini = "", fecha_fin = "";
    try {
        fecha_ini = request.getParameter("fecha_ini");
        fecha_fin = request.getParameter("fecha_fin");
    } catch (Exception e) {

    }
    if (fecha_ini == null) {
        fecha_ini = "";
    }
    if (fecha_fin == null) {
        fecha_fin = "";
    }
%>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/navbar-fixed-top.css" rel="stylesheet">
        <link href="../css/datepicker3.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap.css">
        <!---->
        <title>SIE Sistema de Ingreso de Entradas</title>
    </head>
    <body>
        <div class="container">
            <h1>SIALSS</h1>
            <h4>SISTEMA INTEGRAL DE ADMINISTRACIÓN Y LOGÍSTICA PARA SERVICIOS DE SALUD</h4>

            <%@include file="../jspf/menuPrincipal.jspf" %>

            <div class="panel-heading">
                <h3 class="panel-title">Reporteador Estadística de Entrega</h3>
            </div>
            <form action="../Reporteador" method="post">
                <div class="row">
                    <input type="checkbox" class="btn btn-sm col-lg-offset-0" name="Unidad" id="Unidad" value="F_ClaCli,F_DesUniIS," />UNIDAD  
                    <input type="checkbox" class="btn btn-sm col-lg-offset-0" name="Insumo" id="Insumo" value="F_ClaPro,F_DesPro," />INSUMO
                </div>
                <div class="row">
                    <label class="col-sm-offset-1">Clasificación Unidad de Atención</label>

                    <label class="col-sm-offset-2">Clasificación MEDICAMENTOS</label>
                </div>
                <div class="container">
                    <div class="row col-sm-10">
                        <table class="table col-sm-8">
                            <tr>
                                <!--td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="nivel" id="nivel" />NIVEL  </td-->
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="juris" id="juris" value="F_ClaJurIS,F_DesJurIS," />JS/HOSP    </td>
                                <!--td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="servicio" id="servicio" />SERVICIO    </td>
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="consulta" id="consulta" />CONSULTAS</td-->
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="nivel" id="nivel" value="F_Tipo," />NIVEL</td>
                                <td></td>
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-3" name="origen" id="origen" value="F_Origen," />ORIGEN</td>    
                                <!--td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="catalogo" id="catalogo" />CATALOGO</td>    
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="acredit" id="acredit" />ACREDIT</td>    
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="tipologia" id="tipologia"/>TIPOLOGIA</td-->
                            </tr>
                            <tr>
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="municipio" id="municipio" value="F_ClaMunIS,F_DesMunIS," />MUNICIPIO    </td>
                                <!--td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="modulo" id="modulo" />MODULOS    </td>
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="proyecto" id="proyecto" />PROYECTO    </td>
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="categoria" id="categoria" />CATEGORIA</td-->
                                <td></td>
                                <!--td><input type="checkbox" class="btn btn-sm col-lg-offset-1" name="cobertura" id="cobertura" />COBERTURA</td>    
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="programa" id="programa" />PROGRAMA</td>    
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="partida" id="partida" />PARTIDA</td-->                        
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-offset-2">FACTURAS</label>

                    <label class="col-sm-offset-2">CIFRAS</label>
                </div>
                <div class="container">
                    <div class="row col-sm-8">
                        <table class="table col-sm-3">
                            <tr>
                                <!--td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="tipdoc" id="tipdoc" />TIP DOC    </td-->
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="doct" id="doct" value="F_ClaDoc," />DOCT    </td>
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="lote" id="lote" value="F_ClaLot,F_FecCad," />LOTE    </td>
                                <!--td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="origenl" id="origenl" />ORIGEN LOT</td-->
                                <td></td>
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-3" name="unisur" id="unisur" value="SUM(F_CantSur)," />UNI SUR</td>    
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="unireq" id="unireq" value="SUM(F_CantReq)," />UNI REQ</td>    
                                <td><input type="checkbox" class="btn btn-sm col-lg-offset-0" name="costo" id="costo" value="F_Costo,SUM(monto)," />COSTO</td>                                
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <label class="col-sm-2">Jurisdicción</label>
                        <select name="SelectJuris" id="SelectJuris" class="col-sm-5">
                            <option>--Seleccione--</option>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet Juris = con.consulta("SELECT F_ClaJurIS,CONCAT(F_DesJurIS,' [',F_ClaJurIS,']') AS F_DesJurIS FROM tb_juriis");
                                    while (Juris.next()) {
                            %>
                            <option value="<%=Juris.getString(1)%>"><%=Juris.getString(2)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                }
                            %>
                        </select>
                        <div class="col-sm-4">
                            <input type="text" name="ListJuris" class="col-sm-12" id="ListJuris" readonly="true" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="container">
                    <div class="row">
                        <label class="col-sm-2">Municipio</label>
                        <select name="SelectMuni" id="SelectMuni" class="col-sm-3">
                            <option>--Seleccione--</option>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet Muni = con.consulta("SELECT F_ClaMunIS,CONCAT(F_DesMunIS,' [',F_ClaMunIS,']') AS F_DesMunIS FROM tb_muniis");
                                    while (Muni.next()) {
                            %>
                            <option value="<%=Muni.getString(1)%>"><%=Muni.getString(2)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                }
                            %>
                        </select>
                        <div class="col-sm-4">
                            <input type="text" name="ListMuni" class="col-sm-12" id="ListMuni" readonly="true" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="container">
                    <div class="row">
                        <label class="col-sm-2">Unidad</label>
                        <select name="SelectUnidad" id="SelectUnidad" class="col-sm-5">
                            <option>--Seleccione--</option>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet Unidad = con.consulta("SELECT F_ClaCli,CONCAT(F_ClaCli,' [',F_NomCli,']') AS F_NomCli FROM tb_uniatn");
                                    while (Unidad.next()) {
                            %>
                            <option value="<%=Unidad.getString(1)%>"><%=Unidad.getString(2)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                }
                            %>
                        </select>
                        <div class="col-sm-4">
                            <input type="text" class="col-sm-12" name="ListUni" id="ListUni" readonly="true" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="container">
                    <div class="row">
                        <label class="col-sm-2">Insumo</label>
                        <select name="SelectClave" id="SelectClave" class="col-sm-5">
                            <option>--Seleccione--</option>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet Clave = con.consulta("SELECT F_ClaPro,CONCAT(F_ClaPro,' [',F_DesPro,']') AS F_DesPro FROM tb_medica");
                                    while (Clave.next()) {
                            %>
                            <option value="<%=Clave.getString(1)%>"><%=Clave.getString(2)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                }
                            %>
                        </select>
                        <div class="col-sm-4">
                            <input type="text" class="col-sm-12" name="ListClave" id="ListClave" readonly="true" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="container">
                    <div class="row">
                        <label class="col-sm-2">Tipo Unidad</label>
                        <select name="SelectTipoUnidad" id="SelectTipoUnidad" class="col-sm-2">
                            <option>--Seleccione--</option>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet Clave = con.consulta("SELECT F_Tipo FROM tb_uniatn GROUP BY F_Tipo");
                                    while (Clave.next()) {
                            %>
                            <option value="<%=Clave.getString(1)%>"><%=Clave.getString(1)%></option>
                            <%
                                    }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                }
                            %>
                        </select>
                        <div class="col-sm-4">
                            <input type="text" class="col-sm-12" name="LisTipUni" id="LisTipUni" readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <label class="control-label col-sm-1" for="fecha_ini">Fechas</label>
                        <div class="col-sm-2">
                            <input class="form-control" id="fecha_ini" name="fecha_ini" type="date" onchange="habilitar(this.value);"/>
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" id="fecha_fin" name="fecha_fin" type="date" onchange="habilitar(this.value);"/>
                        </div>
                    </div>   
                </div>        
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <button class="btn btn-block btn-success" id="accion" name="accion" value="Generar" onclick="return confirma();">Generar&nbsp;<label class="glyphicon glyphicon-search"></label></button>                        
                        </div>
                        <div class="col-sm-6">
                            <a class="btn btn-block btn-danger" href="Reporteador.jsp">Limpiar&nbsp;<label class="glyphicon glyphicon-trash"></label></a>                        
                        </div>    
                    </div>
                </div>  
            </form>
        </div>


        <%@include file="../jspf/piePagina.jspf" %>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui-1.10.3.custom.js"></script>
        <script src="../js/bootstrap-datepicker.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>
                                $(document).ready(function() {
                                    $('#datosCompras').dataTable();
                                    $("#fecha").datepicker();
                                    $("#fecha").datepicker('option', {dateFormat: 'dd/mm/yy'});

                                    //$('#btnRecalendarizar').attr('disabled', true);
                                    //$('#btnImpMult').attr('disabled', true);
                                });
        </script>
        <script>
            $(document).ready(function() {
                $('#SelectJuris').change(function() {
                    var Lista = $('#ListJuris').val();
                    var valor = $('#SelectJuris').val();
                    if (Lista != "") {
                        $('#ListJuris').val(Lista + ",'" + valor + "'");
                    } else {
                        $('#ListJuris').val("'" + valor + "'");
                    }
                });
                $('#SelectMuni').change(function() {
                    var Lista = $('#ListMuni').val();
                    var valor = $('#SelectMuni').val();
                    if (Lista != "") {
                        $('#ListMuni').val(Lista + ",'" + valor + "'");
                    } else {
                        $('#ListMuni').val("'" + valor + "'");
                    }
                });
                $('#SelectUnidad').change(function() {
                    var Lista = $('#ListUni').val();
                    var valor = $('#SelectUnidad').val();
                    if (Lista != "") {
                        $('#ListUni').val(Lista + ",'" + valor + "'");
                    } else {
                        $('#ListUni').val("'" + valor + "'");
                    }
                });
                $('#SelectClave').change(function() {
                    var Lista = $('#ListClave').val();
                    var valor = $('#SelectClave').val();
                    if (Lista != "") {
                        $('#ListClave').val(Lista + ",'" + valor + "'");
                    } else {
                        $('#ListClave').val("'" + valor + "'");
                    }
                });

                $('#SelectTipoUnidad').change(function() {
                    var Lista = $('#LisTipUni').val();
                    var valor = $('#SelectTipoUnidad').val();
                    if (Lista != "") {
                        $('#LisTipUni').val(Lista + ",'" + valor + "'");
                    } else {
                        $('#LisTipUni').val("'" + valor + "'");
                    }
                });

            });
        </script>
    </body>
</html>

